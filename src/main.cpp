/*
 * Sample program for DokodeModem
 * ex I2C sample
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>

DOKODEMO Dm = DOKODEMO();
BME280 exSensor = BME280();

void setup()
{
  Dm.begin();            // 初期化が必要です。
  
  Dm.exPowerCtrl(ON);    // 外部電源出力をONにします
  Dm.exComPowerCtrl(ON); // 外部シリアル電源出力をONにします

  I2C_external.begin(); // 外部I2Cを初期化します。100kHzです。

  // 外部デバイスを初期化します。外部I2CにBME280が接続されているとします。
  exSensor.setI2CAddress(BME280_I2C_ADD);
  bool ret = exSensor.beginI2C(I2C_external);
  if (ret)
  {
    SerialDebug.println("init ok");
  }
}

void loop()
{
  // 計測中なら待ちます
  while (!exSensor.isMeasuring())
    delay(1);
  while (exSensor.isMeasuring())
    delay(1);

  // 気温を読み出して表示します
  float deg = exSensor.readTempC();
  SerialDebug.println(String(deg, 1) + "C");

  // １０秒待ちます
  delay(10000);
}
